(* ::Package:: *)

BeginPackage["FormFactors3l`"];


FormFactorBareNonSing::usage=
"FormFactorBareNonSing[CurrentType, EpsilonOrder,  shat] compute the non-singlet contribution 
 to the bare form factors at third order for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s^bare(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorBareNonSing::currenterr=
"The current `1` is unknown";
$FormFactorBareNonSingFailed::usage=
"$FormFactorBareNonSingFailed is returned in case an error occurs.";


FormFactorBareNhSing::usage=
"FormFactorBareNhSing[CurrentType, EpsilonOrder,  shat] compute the nh-singlet contribution
 for the bare form factor at third order for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s^bare(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorBareNhSing::currenterr=
"The current `1` is unknown";
$FormFactorBareNhSingFailed::usage=
"$FormFactorBareNhSingFailed is returned in case an error occurs.";


FormFactorBareNlSing::usage=
"FormFactorBareNlSing[CurrentType, EpsilonOrder,  shat] compute the nl-singlet contribution
 for the bare form factor at third order for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s^bare/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorBareNlSing::currenterr=
"The current `1` is unknown";
$FormFactorBareNlSingFailed::usage=
"$FormFactorBareNhSingFailed is returned in case an error occurs.";


veF1::usage=
"The symbol veF1 is used to identify the form factor F1 of the vector current.";
veF2::usage=
"The symbol veF2 is used to identify the form factor F2 of the vector current.";
axF1::usage=
"The symbol axF1 is used to identify the form factor F1 of the axial-vector current.";
axF2::usage=
"The symbol axF2 is used to identify the form factor F2 of the axial-vector current.";
scF1::usage=
"The symbol scF1 is used to identify the form factor F1 of the scalar current.";
psF1::usage=
"The symbol psF1 is used to identify the form factor F1 of the pseudo-scalar current.";
d33[pR1,pR2];
nc;
XzmOStoMSbar;


FormFactorRenNonSing::usage=
"FormFactorRenNonSing[CurrentType, shat] compute the finite part
 of the non-singlet renormalized and IR subtracted form factor at third order
 for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s(nl)(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorRenNonSing::currenterr=
"The current `1` is unknown";
$FormFactorRenNonSingFailed::usage=
"$FormFactorRenNonSingFailed is returned in case an error occurs.";


FormFactorRenNhSing::usage=
"FormFactorRenNhSing[CurrentType, shat] compute the finite part
 of the nh-singlet renormalized and IR subtracted form factor at third order
 for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s(nl)(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2, scF1, psF1"; 
FormFactorRenNhSing::currenterr=
"The current `1` is unknown";
$FormFactorRenNhSingFailed::usage=
"$FormFactorRenNhSingFailed is returned in case an error occurs.";


FormFactorRenNlSing::usage=
"FormFactorRenNlSing[CurrentType, shat] compute the finite part
 of the nl-singlet renormalized and IR subtracted form factor at third order
 for the current \"CurrentType\" at a given values of shat = s/m^2.
 The result is the third order correction in the expansion parameter Alpha_s(nl)(mu = m)/Pi.
 Possible current types are: veF1, veF2, axF1, axF2"; 
FormFactorRenNlSing::currenterr=
"The current `1` is unknown";
$FormFactorRenNlSingFailed::usage=
"$FormFactorRenNlSingFailed is returned in case an error occurs.";


cR::usage=
"The SU(N) color factor cR = (N^2-1)/(2N).";
cA::usage=
"The SU(N) color factor cA = N.";
I2R::usage=
"The SU(N) color factor I2R = 1/2.";
nh::usage=
"The flag to denote close loops with massive fermions.";
nl::usage=
"The flag to denote close loops with massless fermions.";
nc::usage=
"The number of colors.";
d33::usage=
"The SU(N) color factor d33[V,V] = (N^2-1)(N^2-4)/(16 N)."

$FormFactors3lVersion = "FormFactors3l 1.1 (16 Dec 2022)"

$FormFactors3lDirectory=DirectoryName[$InputFileName];

Print[""];
Print[$FormFactors3lVersion];
Print["by M. Fael, F. Lange, K. Sch\[ODoubleDot]nwald and M. Steinhauser"];
Print["Massive Vector Form Factors to Three Loops"];  
Print["Phys.Rev.Lett. 128 (2022) 17, 172003"]; 
Print["hep-ph/2202.05276\n"]

Print["Singlet and non-singlet three-loop massive form factors"];
Print["Phys.Rev.D 106 (2022) 3, 034029"];
Print["hep-ph/2207.00027\n"];
Print["The functions FormFactorBareNonSing, FormFactorBareNhSing and FormFactorBareNlSing return"];
Print["the values of the bare three-loop form factors"];
Print["for the non-singlet, nh- and nl-singlet contributions, respectively."];
Print[""];
Print["The functions FormFactorRenNonSing, FormFactorRenNhSing and FormFactorRenNlSing return"];
Print["the values of the renormalized and IR-subtracted three-loop form factors"];
Print["for the non-singlet, nh- and nl-singlet contributions, respectively."];


Begin["`Private`"];


(* ::Subsection:: *)
(*Initializations*)


$FormFactors3lDirectory=DirectoryName[$InputFileName];


formfactors={veF1,veF2,axF1,axF2,scF1,psF1};


rangesnonsinglet={{-40,0},{0,3.5},{4.5,5.5},{5.5,16},{16,60}};


rangesnhsinglet={{-40,-2.5},{-2.5,-0.5},{1,3.5},{4.5,5.5},{5.5,16},{16,60}};


rangesnlsinglet={{-40,-2.5},{-2.5,-0.125},{0.125,3.5},{4.5,5.5},{5.5,16},{16,60}};


ChebyshevPolynomial[res_,a_,b_,s_]:=Sum[res[[i]] ChebyshevT[i - 1, x], {i, 1, Length[res]}] - 1/2 res[[1]] /. x -> (2 s - a - b)/(b - a);


(* ::Subsection:: *)
(*Load Grids*)


(* ::Subsubsection:: *)
(*non singlet*)


Do[
	filename=$FormFactors3lDirectory<>"datagrid/grid-nonsing-bare_"<>ToString[formfactors[[nFF]]]<>"_range"<>ToString[rangesnonsinglet[[range,1]]]<>"_"<>ToString[rangesnonsinglet[[range,2]]]<>".m.gz";
	gridnonsingletbare[formfactors[[nFF]],rangesnonsinglet[[range]]] = Import[filename];
,{nFF,1,Length[formfactors]},{range,1,Length[rangesnonsinglet]}];


Do[
	filename=$FormFactors3lDirectory<>"datagrid/grid-nonsing-ren_"<>ToString[formfactors[[nFF]]]<>"_range"<>ToString[rangesnonsinglet[[range,1]]]<>"_"<>ToString[rangesnonsinglet[[range,2]]]<>".m.gz";
	gridnonsingletren[formfactors[[nFF]],rangesnonsinglet[[range]]] = Import[filename];
,{nFF,1,Length[formfactors]},{range,1,Length[rangesnonsinglet]}];


(* ::Subsubsection:: *)
(*nh singlet*)


Do[
	filename=$FormFactors3lDirectory<>"datagrid/grid-nhsing-bare_"<>ToString[formfactors[[nFF]]]<>"_range"<>ToString[rangesnhsinglet[[range,1]]]<>"_"<>ToString[rangesnhsinglet[[range,2]]]<>".m.gz";
	gridnhsingletbare[formfactors[[nFF]],rangesnhsinglet[[range]]] = Import[filename];
,{nFF,1,Length[formfactors]},{range,1,Length[rangesnhsinglet]}];


Do[
	filename=$FormFactors3lDirectory<>"datagrid/grid-nhsing-ren_"<>ToString[formfactors[[nFF]]]<>"_range"<>ToString[rangesnhsinglet[[range,1]]]<>"_"<>ToString[rangesnhsinglet[[range,2]]]<>".m.gz";
	gridnhsingletren[formfactors[[nFF]],rangesnhsinglet[[range]]] = Import[filename];
,{nFF,1,Length[formfactors]},{range,1,Length[rangesnhsinglet]}];


(* ::Subsubsection:: *)
(*nl singlet*)


Do[
	filename=$FormFactors3lDirectory<>"datagrid/grid-nlsing-bare_"<>ToString[formfactors[[nFF]]]<>"_range"<>ToString[rangesnlsinglet[[range,1]]]<>"_"<>ToString[rangesnlsinglet[[range,2]]]<>".m.gz";
	gridnlsingletbare[formfactors[[nFF]],rangesnlsinglet[[range]]] = Import[filename];
,{nFF,1,Length[formfactors[[1;;4]]]},{range,1,Length[rangesnlsinglet]}];


Do[
	filename=$FormFactors3lDirectory<>"datagrid/grid-nlsing-ren_"<>ToString[formfactors[[nFF]]]<>"_range"<>ToString[rangesnlsinglet[[range,1]]]<>"_"<>ToString[rangesnlsinglet[[range,2]]]<>".m.gz";
	gridnlsingletren[formfactors[[nFF]],rangesnlsinglet[[range]]] = Import[filename];
,{nFF,1,Length[formfactors[[1;;4]]]},{range,1,Length[rangesnlsinglet]}];


(* ::Subsection:: *)
(*Load expansions*)


(* ::Subsubsection:: *)
(*non singlet*)


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nonsing-bare-inf-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	seriesinfnonsingletbare[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nonsing-ren-inf-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	seriesinfnonsingletren[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nonsing-bare-4-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series4nonsingletbare[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nonsing-ren-4-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series4nonsingletren[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


(* ::Subsubsection:: *)
(*nh singlet*)


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nhsing-bare-inf-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	seriesinfnhsingletbare[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nhsing-ren-inf-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	seriesinfnhsingletren[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nhsing-bare-0-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series0nhsingletbare[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nhsing-ren-0-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series0nhsingletren[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nhsing-bare-4-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series4nhsingletbare[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nhsing-ren-4-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series4nhsingletren[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]}];


(* ::Subsubsection:: *)
(*nl singlet*)


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nlsing-bare-inf-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	seriesinfnlsingletbare[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]-2}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nlsing-ren-inf-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	seriesinfnlsingletren[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]-2}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nlsing-bare-0-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series0nlsingletbare[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]-2}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nlsing-ren-0-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series0nlsingletren[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]-2}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nlsing-bare-4-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series4nlsingletbare[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]-2}];


Do[
	filename = $FormFactors3lDirectory<>"expansions/series-nlsing-ren-4-"<>ToString[formfactors[[nFF]]]<>".m.gz";
	series4nlsingletren[formfactors[[nFF]]] = Import[filename];
,{nFF,1,Length[formfactors]-2}];


(* ::Subsection:: *)
(*Function for bare form factors*)


FormFactorBareNonSing[CurrentType_Symbol, EpsilonOrder_Integer,  s_?NumericQ]:=
Block[{VariableChangeInverseInf,VariableChangeInverse4,range,result},
	VariableChangeInverseInf = {xsminf->-(6/(-10+ss))};
	VariableChangeInverse4 = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorBareNonSing::currenterr,CurrentType]; Return[$FormFactorBareNonSingFailed];];
	If[EpsilonOrder > 0 || EpsilonOrder < -3,Return[$FormFactorBareNonSingFailed];];
	result = $FormFactorBareNonSingFailed;
	Do[
		range = rangesnonsinglet[[i]];
		If[IntervalMemberQ[ Interval[range],s],
			result=Expand[ChebyshevPolynomial[gridnonsingletbare[CurrentType,range][[EpsilonOrder+4]],range[[1]],range[[2]],s]];
			Break[];
		];
	,{i,1,Length[rangesnonsinglet]}];
	Which[s<-40 || s> 60, result=Expand[Coefficient[seriesinfnonsingletbare[CurrentType],ep,EpsilonOrder]/.lx->Log[xsminf]/.(VariableChangeInverseInf/.ss->s)],
		3.5<=s<4.5,result=Expand[Coefficient[series4nonsingletbare[CurrentType],ep,EpsilonOrder]/.ls->Log[sqrt4ms]/.(VariableChangeInverse4/.ss->s)]
	];
	result	
];


FormFactorBareNhSing[CurrentType_Symbol, EpsilonOrder_Integer,  s_?NumericQ]:=
Block[{VariableChangeInverseInf,VariableChangeInverse4,range,result,VariableChangeInverse0},
	VariableChangeInverseInf = {xsminf->(8/(8-ss))};
	VariableChangeInverse4 = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	VariableChangeInverse0 = {sqrtms->-Sqrt[-ss] Sign[ss]};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorBarenNhSing::currenterr,CurrentType]; Return[$FormFactorBareNhSingFailed];];
	If[EpsilonOrder > 0 || EpsilonOrder < -3,Return[$FormFactorBareNhSingFailed];];
	result = $FormFactorBareNhSingFailed;
	Do[
		range = rangesnhsinglet[[i]];
		If[IntervalMemberQ[ Interval[range],s],
			result=Expand[ChebyshevPolynomial[gridnhsingletbare[CurrentType,range][[EpsilonOrder+4]],range[[1]],range[[2]],s]];
			Break[];
		];
	,{i,1,Length[rangesnhsinglet]}];
	Which[s<-40 || s> 60, result=Expand[Coefficient[seriesinfnhsingletbare[CurrentType],ep,EpsilonOrder]/.lx->Log[xsminf]/.(VariableChangeInverseInf/.ss->s)],
		-0.5<=s<1.0,result=Expand[Coefficient[series0nhsingletbare[CurrentType],ep,EpsilonOrder]/.ls->Log[sqrtms]/.(VariableChangeInverse0/.ss->s)],
		3.5<=s<4.5,result=Expand[Coefficient[series4nhsingletbare[CurrentType],ep,EpsilonOrder]/.ls->Log[sqrt4ms]/.(VariableChangeInverse4/.ss->s)]
	];
	result	
];


FormFactorBareNlSing[CurrentType_Symbol, EpsilonOrder_Integer,  s_?NumericQ]:=
Block[{VariableChangeInverseInf,VariableChangeInverse4,range,result,VariableChangeInverse0},
	VariableChangeInverseInf = {xsminf->(2/(2-ss))};
	VariableChangeInverse4 = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	VariableChangeInverse0 = {sqrtms->-Sqrt[-ss] Sign[ss]};
	If[!MemberQ[formfactors[[1;;4]],CurrentType],Message[FormFactorBarenNlSing::currenterr,CurrentType]; Return[$FormFactorBareNlSingFailed];];
	If[EpsilonOrder > 0 || EpsilonOrder < -3,Return[$FormFactorBareNlSingFailed];];
	result = $FormFactorBareNlSingFailed;
	Do[
		range = rangesnlsinglet[[i]];
		If[IntervalMemberQ[ Interval[range],s],
			result=Expand[ChebyshevPolynomial[gridnlsingletbare[CurrentType,range][[EpsilonOrder+4]],range[[1]],range[[2]],s]];
			Break[];
		];
	,{i,1,Length[rangesnlsinglet]}];
	Which[s<-40 || s> 60, result=Expand[Coefficient[seriesinfnlsingletbare[CurrentType],ep,EpsilonOrder]/.lx->Log[xsminf]/.(VariableChangeInverseInf/.ss->s)],
		-0.125<=s<0.125,result=Expand[Coefficient[series0nlsingletbare[CurrentType],ep,EpsilonOrder]/.ls->Log[sqrtms]/.(VariableChangeInverse0/.ss->s)],
		3.5<=s<4.5,result=Expand[Coefficient[series4nlsingletbare[CurrentType],ep,EpsilonOrder]/.ls->Log[sqrt4ms]/.(VariableChangeInverse4/.ss->s)]
	];
	result	
];


(* ::Subsection:: *)
(*Function Renormalized Form Factors*)


FormFactorRenNonSing[CurrentType_Symbol, s_?NumericQ]:=
Block[{VariableChangeInverseInf,VariableChangeInverse4,EpsilonOrder,repx2s,result},
	EpsilonOrder = 0;
	VariableChangeInverseInf = {xsminf->-(6/(-10+ss))};
	VariableChangeInverse4  = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	repx2s = {x -> (2 + Sqrt[-4 + ss]*Sqrt[ss] - ss)/2};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorRenNonSing::currenterr,CurrentType]; Return[$FormFactorRenNonSingFailed];];
	result = $FormFactorRenNonSingFailed;
	Do[
		range = rangesnonsinglet[[i]];
		If[IntervalMemberQ[ Interval[range],s],
			result=Expand[ChebyshevPolynomial[gridnonsingletren[CurrentType,range],range[[1]],range[[2]],s]];
			Break[];
		];
	,{i,1,Length[rangesnonsinglet]}];
	Which[s<-40 || s> 60, result=Expand[seriesinfnonsingletren[CurrentType]/.lx->Log[xsminf]/.(VariableChangeInverseInf/.ss->s)],
			3.5<=s<4.5,    result=Expand[series4nonsingletren[CurrentType]/.ls->Log[sqrt4ms]/.(VariableChangeInverse4/.ss->s)]];
	result
]; 


FormFactorRenNhSing[CurrentType_Symbol, s_?NumericQ]:=
Block[{VariableChangeInverseInf,VariableChangeInverse4,EpsilonOrder,repx2s,result,VariableChangeInverse0},
	EpsilonOrder = 0;
	VariableChangeInverseInf = {xsminf->(8/(8-ss))};
	VariableChangeInverse4  = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	VariableChangeInverse0 = {sqrtms->-Sqrt[-ss] Sign[ss]};
	repx2s = {x -> (2 + Sqrt[-4 + ss]*Sqrt[ss] - ss)/2};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorRenNhSing::currenterr,CurrentType]; Return[$FormFactorRenNhSingFailed];];
	result = $FormFactorRenNhSingFailed;
	Do[
		range = rangesnhsinglet[[i]];
		If[IntervalMemberQ[ Interval[range],s],
			result=Expand[ChebyshevPolynomial[gridnhsingletren[CurrentType,range],range[[1]],range[[2]],s]];
			Break[];
		];
	,{i,1,Length[rangesnhsinglet]}];
	Which[s<-40 || s> 60, result=Expand[seriesinfnhsingletren[CurrentType]/.lx->Log[xsminf]/.(VariableChangeInverseInf/.ss->s)],
			-0.5<=s<1.0,result=Expand[series0nhsingletren[CurrentType]/.ls->Log[sqrtms]/.(VariableChangeInverse0/.ss->s)],
			3.5<=s<4.5,result=Expand[series4nhsingletren[CurrentType]/.ls->Log[sqrt4ms]/.(VariableChangeInverse4/.ss->s)]];
	result
]; 


FormFactorRenNlSing[CurrentType_Symbol, s_?NumericQ]:=
Block[{VariableChangeInverseInf,VariableChangeInverse4,EpsilonOrder,repx2s,result,VariableChangeInverse0},
	EpsilonOrder = 0;
	VariableChangeInverseInf = {xsminf->(2/(2-ss))};
	VariableChangeInverse4  = {sqrt4ms->Sign[4-ss]Sqrt[4-ss]};
	VariableChangeInverse0 = {sqrtms->-Sqrt[-ss] Sign[ss]};
	repx2s = {x -> (2 + Sqrt[-4 + ss]*Sqrt[ss] - ss)/2};
	If[!MemberQ[formfactors,CurrentType],Message[FormFactorRenNlSing::currenterr,CurrentType]; Return[$FormFactorRenNlSingFailed];];
	result = $FormFactorRenNlSingFailed;
	Do[
		range = rangesnlsinglet[[i]];
		If[IntervalMemberQ[ Interval[range],s],
			result=Expand[ChebyshevPolynomial[gridnlsingletren[CurrentType,range],range[[1]],range[[2]],s]];
			Break[];
		];
	,{i,1,Length[rangesnlsinglet]}];
	Which[s<-40 || s> 60, result=Expand[seriesinfnlsingletren[CurrentType]/.lx->Log[xsminf]/.(VariableChangeInverseInf/.ss->s)],
			-0.125<=s<0.125,result=Expand[series0nlsingletren[CurrentType]/.ls->Log[sqrtms]/.(VariableChangeInverse0/.ss->s)],
			3.5<=s<4.5,result=Expand[series4nlsingletren[CurrentType]/.ls->Log[sqrt4ms]/.(VariableChangeInverse4/.ss->s)]];
	result
]; 


(* ::Subsection::Closed:: *)
(*Epilogue*)


End[];


EndPackage[];

# FormFactors3l

FormFactors3l is a Mathematica package that evaluates the third-order corrections to the QCD form factors.
It uses interpolated grids for the evaluation of the form factors in the range $`-40 < s/m^2 < 3.5`$
and $`4.5 < s/m^2 < 60`$. 
In the remaining regions, it uses special series expansion around $`s=\pm \infty`$ and $`s=4`$ (also $`s=0`$ for the singlet contributions).

It is based on the results presented in:

M. Fael, F. Lange, K. Schönwald, M. Steinhauser     
Massive Vector Form Factors to Three Loops     
Phys.Rev.Lett. 128 (2022) 17, 172003     
[arXiv: 2202.05276 [hep-ph]]    

Singlet and non-singlet three-loop massive form factors     
Phys.Rev.D 106 (2022) 3, 034029     
[arXiv: 2207.00027 [hep-ph]]    

Massive three-loop form factors: Anomaly contribution     
arXiv: 2302.00693 [hep-ph]    

## Getting started

The package can be loaded in a Mathematica session with the command
```
Get["PATH/FormFactors3l.m"]
```
provided the `PATH` points to the directory of the package. All
functionality is contained in this file, there are no dependencies other than Mathematica itself.
The package has been tested on a Linux and macOS operating systems with the Mathematica versions 8 to 13.

## Bare form factors at three loops

The functions `FormFactorBareNonSing[CurrentType, EpsilonOrder, s]`, `FormFactorBareNhSing[CurrentType, EpsilonOrder, s]` 
and  `FormFactorBareNlSing[CurrentType, EpsilonOrder, s]`
return the non-singlet, nh- and nl-singlet contributions to the bare form factors at three loops, respectively.
The result is the third-order correction in the expansion parameter $`\alpha_s^{bare}/\pi`$.
The various form factors are denoted by `CurrentType = {veF1, veF2, axF1, axF2, scF1, psF1}`.
The order in the dimensional regulator $`\epsilon = (4-d)/2`$ is denoted by `EpsilonOrder`.

Example:
```
In[]  := FormFactorBareNonSing[veF1, 0, -1]
Out[] := 77.0506 cA^2 cR+95.0634 cA cR^2+0.467466 cR^3
        -21.9243 cA cR I2R nh-11.5582 cR^2 I2R nh+0.751403 cR I2R^2 nh^2
        -62.6063 cA cR I2R nl-45.5408 cR^2 I2R nl+9.35837 cR I2R^2 nh nl
        +11.8102 cR I2R^2 nl^2
```

## Renormalized form factors at three loops

The functions `FormFactorRenNonSing[CurrentType, s]`, `FormFactorRenNonSing[CurrentType, s]`  and `FormFactorRenNlSing[CurrentType, s]` return
the values of finite part in the epsilon expansion for the renormalized and IR-subtracted non-singlet, nh- and nl-singlet form factors, respectively.
For the exact definition see Sec. 5 of hep-ph/2207.00027.
The result is the third order correction in the expansion parameter $`\alpha_s^{(n_l)}/\pi`$,
with the renormalization scale equal to the mass of the quark: $`\mu=m`$.

Example:
```
In[]  := FormFactorRenNonSing[veF1, -1]
Out[] := 3.10714 cA^2 cR-3.23413 cA cR^2+0.0144347 cR^3
        +0.0435081 cA cR I2R nh-0.0640418 cR^2 I2R nh
        -0.0107609 cR I2R^2 nh^2-2.59041 cA cR I2R nl
        +1.02032 cR^2 I2R nl+0.000282528 cR I2R^2 nh nl
        +0.494057 cR I2R^2 nl^2
```
